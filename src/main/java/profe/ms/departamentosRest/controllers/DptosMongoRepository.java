package profe.ms.departamentosRest.controllers;

import org.springframework.data.mongodb.repository.MongoRepository;

import profe.empleados.model.Departamento;

public interface DptosMongoRepository extends MongoRepository<Departamento, String> {

}