package profe.ms.empleadosweb.exceptions;

public class EmpleadosException extends RuntimeException {

	public EmpleadosException() {
		// TODO Auto-generated constructor stub
	}

	public EmpleadosException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmpleadosException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadosException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadosException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
